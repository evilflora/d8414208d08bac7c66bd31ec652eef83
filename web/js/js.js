﻿jQuery(document).ready(function(){

	//-------------------------
	var xhr = new Array(10);
	for (var i = 0; i < 10; i++) {
		xhr[i] = 0;
	}

	/*navigation.php toto a refaire */
	$("ul.subMenu").hide();
	$("ul.subMenu").first().show();
	$(".ep_list .sub_list div .navigation").click( function () {
		if ($(this).closest(".sub_list").next("ul.subMenu:visible").length == 0 && $(".sub_list").length > 1) {
			$(this).closest(".ep_list").find(".subMenu").slideUp("normal");
		}
		$(this).closest(".sub_list").find(".subMenu").slideDown("normal");

		return false;
	});

	/*scroll*/
	if($('.news:last').length) {
		var load = false;
		var offset = $('.news:last').offset();
		$(window).scroll(function(){
			if((offset.top-$(window).height() <= $(window).scrollTop())
				&& load==false && ($('.news').size()>=2)){
				load = true;
				$('.loadmore_news').show();
				$.ajax({
					url: 'includes/scroll/ajax_news.php',
					type: 'get',
					success: function(data) {
						$('.loadmore_news').fadeOut(500);
						$('.news:last').after(data);
						offset = $('.news:last').offset();
						load = false;
					}
				});
			}
		});
	}

	if($('.commentary:last').length) {
		var load = false;
		var offset = $('.commentary:last').offset();
		$(window).scroll(function(){
			if((offset.top-$(window).height() <= $(window).scrollTop())
				&& load==false && ($('.commentary').size()>=2)){
				load = true;
				$('.loadmore_coms').show();
				$.ajax({
					url: 'includes/scroll/ajax_comment.php',
					type: 'get',
					success: function(data) {
						$('.loadmore_coms').fadeOut(500);
						$('.commentary:last').after(data);
						offset = $('.commentary:last').offset();
						load = false;
					}
				});
			}
		});
	}

	/*validation engine*/
	//jQuery(".validate").validationEngine(); // todo

	/* dataTable */
	/*$('#series_list, #king_list').dataTable( {
		"iDisplayLength": 10,
		"aaSorting": [[ 0, "asc" ]],
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Tout"]]
	});
	$('#news_list, #members_list, #ip_list, #categories_list, #logs_list').dataTable( {
		"iDisplayLength": 25,
		"aaSorting": [[ 0, "desc" ]],
		"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Tout"]]
	});*/

	// Si ajout dynamique rajouter ceci
	/*$( document ).on( "click", ".validate",function() {
		jQuery(".validate").validationEngine();
	});*/

	$( document ).on( "click", "input[name='cancel_remind_data']",function() {
		$(".remind_data").remove();
		return false;
	});

	/*account.php*/
	$( document ).on( "click", "#form_edit_pwd input[type='submit']",function() {
		if(xhr[2] == 0)
		{
			$(this).closest('form').first().hide();
			$(".load_form_edit_password").css("display","block");
			if($("#form_change_password").length == 0)
			{
				xhr[2] = $.post( "includes/body/form_change_password.php", { },
					function(data) { $("#form_edit_pwd").first().after(data); });
			}
			xhr[2].always(function() {
				$(".load_form_edit_password").css("display","none");
				xhr[2] = 0;
			});
			xhr[2].done(function() {
				//
			});
			xhr[2].fail(function() {
				$(this).closest('form').first().show();
				alert('Erreur lors de la récupération du fichier');
			});
		}
		return false;
	});
	$( document ).on( "click", "#form_change_password input[type='submit']",function() {
		if(xhr[3] == 0)
		{
			$(".load_form_edit_password").css("display","block");
			xhr[3] = $.post( "includes/body/upload_new_password.php", { last_password : $("input[name='last_password']").val(), news_pwd : $("input[name='news_pwd']").val(), confirm_pwd : $("input[name='confirm_pwd']").val(), token : $("input:hidden[name='token']").val()},
				function(data) { $("h1").first().after(data); });
			xhr[3].done(function() {
				$("#form_edit_pwd").show();
				$("#form_change_password").remove();
			});
			xhr[3].fail(function() {
				alert('Erreur lors de la récupération du fichier');
			});
			xhr[3].always(function() {
				$(".load_form_edit_password").css("display","none");
				xhr[3] = 0;
			});
		}
		return false;
	});
	$( document ).on( "click", "#form_change_password input[type='button']",function() {
		$("#form_edit_pwd").show();
		$("#form_change_password").remove();
		return false;
	});

	/*Gestion utilisateurs*/
	$( ".editer_titre" ).submit(function( ) {
		$("#edit_user_title").remove();

		$.post( "includes/body/form_edit_user_title.php", {
				id : $( "input:hidden[name='user_id']", $( this) ).attr( "value" )},
			function(data) { $("h1").first().after(data); });

		return false;
	});
	$( document ).on( "click", "input[type='button']",function() {
		$(this).closest('form').first().remove();
		return false;
	});

	/*Bannir membre*/
	$( ".bannir_id" ).submit(function( ) {
		$("#action_bann_id").remove();

		$.post( "includes/body/form_bann_id.php", {
				id : $( "input:hidden[name='user_id']", $( this) ).attr( "value" )},
			function(data) { $("h1").first().after(data); });

		return false;
	});

	/* Bannir IP */
	$( ".bannir_ip" ).submit(function( ) {
		$("#form_bann_ip").remove();

		$.post( "includes/body/form_bann_ip.php", {
				ip : $( "input:hidden[name='user_ip']", $( this) ).attr( "value" )},
			function(data) { $("h1").first().after(data); });

		return false;
	});

	/* Selection des épisodes vus */
	$( document ).on( "click", ".choix",function() {
		var checked = true;
		var name = this.id;

		$("input[name='"+name+"[]']").each(function() {
			if (this.checked == false)
				checked = false;
		});

		$("#"+name)[0].checked = checked;
	});
	$( document ).on( "click", ".checkbox",function() {
		var checked = true;

		if (this.checked != true) {
			checked = false;
		}

		var cases_to_check = $(this).attr( "id" );

		$("input[name='"+cases_to_check+"[]']").each(function() {
			this.checked = checked;
		});
	});

	$( document ).on( "click", ".post_comment input[type='submit']",function() {
		if(xhr[5] == 0) {
			var url = $(this).closest("form");
			xhr[6] = $.post( "includes/body/post_comment.php", { msg : url.find("textarea").val(), token : url.find("input:hidden[name='token']").val() },
				function(data) { $(".commentaries_list").prepend(data); });
			xhr[6].done(function() {
				if($(".erreur").length > 1) {
					$(".erreur").first().remove();
				}
				if(!$("#information").length) {
					if($(".information").first().length) {
						$(".information").first().remove();
					}
				}
				url.find("textarea").val("");
			});
			xhr[6].fail(function() {
				alert('Erreur lors de la récupération du fichier');
			});
			xhr[6].always(function() {
				xhr[6] = 0;
			});
		}
		return false;
	});
	$( document ).on( "click", ".update_comment",function() {
		if(!$('.modify_comment').length) {
			if(xhr[5] == 0) {
				var url = $( this ).closest('.commentary');

				xhr[5] = $.post( "includes/body/modify_commentary.php", { texte : url.find(".commentary_body div").text(), id : url.attr('id') },
					function(data) { url.find(".commentary_body").after(data); });
				xhr[5].done(function() {
					url.find(".commentary_body").hide();
				});
				xhr[5].fail(function() {
					alert('Erreur lors de la récupération du fichier');
					url.find(".commentary_body").show();
				});
				xhr[5].always(function() {
					xhr[5] = 0;
				});
			}
		}
		return false;
	});
	$( document ).on( "click", ".delete_comment",function() {
		if(confirm('Vous êtes sûr ?')) {
			if(confirm('Vraiment ?')) {
				if(xhr[5] == 0) {
					var url = $( this ).closest('.commentary');

					xhr[5] = $.post( "includes/body/delete_comment.php", { id : url.attr('id') },
						function(data) { url.find(".commentary_body").after(data); });
					xhr[5].done(function() {
						url.find(".commentary_body div").first().remove();
					});
					xhr[5].fail(function() {
						alert('Erreur lors de la récupération du fichier');
						url.find(".commentary_body").show();
					});
					xhr[5].always(function() {
						xhr[5] = 0;
					});
				}
			}
		}
		return false;
	});
	$( document ).on( "click", "input[name='cancel_update_comment']",function() {
		$( this ).closest('.commentary').find(".commentary_body").show();
		$( ".modify_comment" ).remove();
		return false;
	});
	$( document ).on( "click", "input[name='enregistrer_modif_coms']",function() {
		if($('.modify_comment').length) {
			if(xhr[5] == 0) {
				if ($("#information").length)
					$("#information").remove();
				var _parent = $( this ).closest('form');
				var token = $(_parent).find('input:hidden[name=token]').val();
				var msg = $(_parent).find("textarea").val();
				xhr[5] = $.post( "includes/body/upload_modified_comment.php", { texte : msg , token : token},
					function(data) { $("section h1").after(data); });
				xhr[5].done(function() {
					if(!$("section div").first().hasClass("erreur")) {
						$(_parent).closest('.commentary').find(".commentary_body div").text(msg);
						$(_parent).closest('.commentary').find(".commentary_body").show();
						$(_parent).remove();
					}
				});
				xhr[5].fail(function() {
					alert('Erreur lors de la récupération du fichier');
				});
				xhr[5].always(function() {
					xhr[5] = 0;
				});
			}
		}
		return false;
	});

	$( document ).on( "click", "input[name='removable']",function() {
		$(this).closest('.removable').first().remove();
		return false;
	});

	/*$( document ).on( "ready", "",function() {
		$("script").remove();
	});*/
	$( document ).on( "click", ".close",function() {
		$(this).remove();
	});

	$( document ).ready(function() {
		$(this).remove();
	});

	$( document ).on( "click", "input",function() {
		if($(this).attr('onclick') !== undefined) {
			if($(this).attr('onclick').length == 0)
				if(confirm('Vous êtes sûr ?')) {
					if(confirm('Vraiment ?')) {
						return true;
					}
				}
			return false;
		}
	});

	$( document ).on( "click", "input[type=reset]",function() {
		location.replace(location.pathname);
	});
});