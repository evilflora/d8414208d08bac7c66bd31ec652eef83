<?php

namespace DejaVuBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Series
 *
 * @ORM\Entity(repositoryClass="SeriesRepository")
 */
class Series
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=128, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="alternativ_title", type="string", length=128, nullable=true)
     */
    private $alternativTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="authors", type="string", length=128, nullable=false)
     */
    private $authors;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_finish", type="boolean", nullable=false)
     */
    private $finished = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="url_img", type="string", length=256, nullable=true)
     */
    private $urlImg;


    /**
     * @var Licensee
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\Licensee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="licensee_id", referencedColumnName="id")
     * })
     */
    private $license;

    /**
     * @var \DejaVuBundle\Entity\Type
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\Type")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="DejaVuBundle\Entity\Kind")
     * @ORM\JoinTable(name="series_has_kind",
     *   joinColumns={
     *     @ORM\JoinColumn(name="serie_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="kind_id", referencedColumnName="id")
     *   }
     * )
     */
    private $kind;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->kind = new ArrayCollection();
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Series
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set alternativTitle
     *
     * @param string $alternativTitle
     *
     * @return Series
     */
    public function setAlternativTitle($alternativTitle)
    {
        $this->alternativTitle = $alternativTitle;

        return $this;
    }

    /**
     * Get alternativTitle
     *
     * @return string
     */
    public function getAlternativTitle()
    {
        return $this->alternativTitle;
    }

    /**
     * Set authors
     *
     * @param string $authors
     *
     * @return Series
     */
    public function setAuthors($authors)
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * Get authors
     *
     * @return string
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Series
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Finished
     *
     * @param boolean $finished
     *
     * @return Series
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * Get Finished
     *
     * @return boolean
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * Set urlImg
     *
     * @param string $urlImg
     *
     * @return Series
     */
    public function setUrlImg($urlImg)
    {
        $this->urlImg = $urlImg;

        return $this;
    }

    /**
     * Get urlImg
     *
     * @return string
     */
    public function getUrlImg()
    {
        return $this->urlImg;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getid()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param \DejaVuBundle\Entity\Type $type
     *
     * @return Series
     */
    public function setType(Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \DejaVuBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add kind
     *
     * @param \DejaVuBundle\Entity\Type $kind
     *
     * @return Series
     */
    public function addKind(Type $kind)
    {
        $this->kind[] = $kind;

        return $this;
    }

    /**
     * Remove kind
     *
     * @param \DejaVuBundle\Entity\Type $kind
     */
    public function removeKind(Type $kind)
    {
        $this->kind->removeElement($kind);
    }

    /**
     * Get kind
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Set license
     *
     * @param Licensee $license
     *
     * @return Series
     */
    public function setLicense(Licensee $license = null)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license
     *
     * @return Licensee
     */
    public function getLicense()
    {
        return $this->license;
    }
}
