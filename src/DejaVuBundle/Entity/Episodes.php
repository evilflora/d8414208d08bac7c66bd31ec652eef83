<?php

namespace DejaVuBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Episodes
 *
 * @ORM\Entity(repositoryClass="EpisodesRepository")
 */
class Episodes
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="episode", type="smallint", nullable=false)
     */
    private $episode;

    /**
     * @var Seasons
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\Seasons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="season_id", referencedColumnName="id")
     * })
     */
    private $season;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="DejaVuBundle\Entity\Users", mappedBy="episode")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    /**
     * Set episode
     *
     * @param integer $episode
     *
     * @return Episodes
     */
    public function setEpisode($episode)
    {
        $this->episode = $episode;

        return $this;
    }

    /**
     * Get episode
     *
     * @return integer
     */
    public function getEpisode()
    {
        return $this->episode;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getid()
    {
        return $this->id;
    }

    /**
     * Set season
     *
     * @param Seasons $season
     *
     * @return Episodes
     */
    public function setSeason(Seasons $season = null)
    {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return Seasons
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Add user
     *
     * @param Users $user
     *
     * @return Episodes
     */
    public function addUser(Users $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param Users $user
     */
    public function removeUser(Users $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
