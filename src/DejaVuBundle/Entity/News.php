<?php

namespace DejaVuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * News
 *
 * @ORM\Entity(repositoryClass="NewsRepository")
 */
class News
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=40, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="post_date", type="datetime", nullable=false)
     */
    private $postDate;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="editer_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $editer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="editer_date", type="datetime", nullable=true)
     */
    private $editerDate;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->postDate = new \Datetime();
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return News
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set postDate
     *
     * @param \DateTime $postDate
     *
     * @return News
     */
    public function setPostDate($postDate)
    {
        $this->postDate = $postDate;

        return $this;
    }

    /**
     * Get postDate
     *
     * @return \DateTime
     */
    public function getPostDate()
    {
        return $this->postDate;
    }

    /**
     * Set editerDate
     *
     * @param \DateTime $editerDate
     *
     * @return News
     */
    public function setEditerDate($editerDate)
    {
        $this->editerDate = $editerDate;

        return $this;
    }

    /**
     * Get editerDate
     *
     * @return \DateTime
     */
    public function getEditerDate()
    {
        return $this->editerDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getid()
    {
        return $this->id;
    }

    /**
     * Set editer
     *
     * @param \DejaVuBundle\Entity\Users $editer
     *
     * @return News
     */
    public function setEditer(Users $editer = null)
    {
        $this->editer = $editer;

        return $this;
    }

    /**
     * Get editer
     *
     * @return \DejaVuBundle\Entity\Users
     */
    public function getEditer()
    {
        return $this->editer;
    }

    /**
     * Set user
     *
     * @param \DejaVuBundle\Entity\Users $user
     *
     * @return News
     */
    public function setUser(Users $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DejaVuBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
