<?php

namespace DejaVuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BannedIp
 *
 * @ORM\Entity(repositoryClass="BannedIpRepository")
 */
class BannedIp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=15, nullable=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="banned", type="text", length=65535, nullable=true)
     */
    private $banned;

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set banned
     *+
     * @param string $banned
     *
     * @return BannedIp
     */
    public function setBanned($banned)
    {
        $this->banned = $banned;

        return $this;
    }

    /**
     * Get banned
     *
     * @return string
     */
    public function getBanned()
    {
        return $this->banned;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return BannedIp
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }
}
