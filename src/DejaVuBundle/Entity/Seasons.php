<?php

namespace DejaVuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seasons
 *
 * @ORM\Entity(repositoryClass="SeasonsRepository")
 */
class Seasons
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="season", type="smallint", nullable=false)
     */
    private $season;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="smallint", nullable=false)
     */
    private $year;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_finish", type="boolean", nullable=false)
     */
    private $finished = '0';

    /**
     * @var \DejaVuBundle\Entity\Series
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\Series")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="serie_id", referencedColumnName="id")
     * })
     */
    private $series;

    /**
     * Set season
     *
     * @param integer $season
     *
     * @return Seasons
     */
    public function setSeason($season)
    {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return integer
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return Seasons
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set Finished
     *
     * @param boolean $finished
     *
     * @return Seasons
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * Get Finished
     *
     * @return boolean
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getid()
    {
        return $this->id;
    }

    /**
     * Set series
     *
     * @param \DejaVuBundle\Entity\Series $series
     *
     * @return Seasons
     */
    public function setSeries(Series $series = null)
    {
        $this->series = $series;

        return $this;
    }

    /**
     * Get series
     *
     * @return \DejaVuBundle\Entity\Series
     */
    public function getSeries()
    {
        return $this->series;
    }
}
