<?php

namespace DejaVuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comments
 *
 * @ORM\Entity(repositoryClass="CommentsRepository")
 */
class Comments
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="post_date", type="datetime", nullable=false)
     */
    private $postDate;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="editer_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $editer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="editer_date", type="datetime", nullable=true)
     */
    private $editerDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted;

    /**
     * @var News
     *
     * @ORM\ManyToOne(targetEntity="DejaVuBundle\Entity\News")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="news_id", referencedColumnName="id")
     * })
     */
    private $news;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->postDate = new \Datetime();
        $this->deleted = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Comments
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set postDate
     *
     * @param \DateTime $postDate
     *
     * @return Comments
     */
    public function setPostDate($postDate)
    {
        $this->postDate = $postDate;

        return $this;
    }

    /**
     * Get postDate
     *
     * @return \DateTime
     */
    public function getPostDate()
    {
        return $this->postDate;
    }

    /**
     * Set editerDate
     *
     * @param \DateTime $editerDate
     *
     * @return Comments
     */
    public function setEditerDate($editerDate)
    {
        $this->editerDate = $editerDate;

        return $this;
    }

    /**
     * Get editerDate
     *
     * @return \DateTime
     */
    public function getEditerDate()
    {
        return $this->editerDate;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Comments
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set user
     *
     * @param Users $user
     *
     * @return Comments
     */
    public function setUser(Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return Users
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set editer
     *
     * @param Users $editer
     *
     * @return Comments
     */
    public function setEditer(Users $editer = null)
    {
        $this->editer = $editer;

        return $this;
    }

    /**
     * Get editer
     *
     * @return Users
     */
    public function getEditer()
    {
        return $this->editer;
    }

    /**
     * Set news
     *
     * @param News $news
     *
     * @return Comments
     */
    public function setNews(News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return News
     */
    public function getNews()
    {
        return $this->news;
    }
}
