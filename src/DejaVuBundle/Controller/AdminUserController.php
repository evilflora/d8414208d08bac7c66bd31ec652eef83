<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\Titles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminUserController extends Controller
{
    /*
     * Route nomdusite.fr/admin/members
     */
    public function viewAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:admin/members.html.twig', array(
            'users'             => $bdd->getRepository('DejaVuBundle:Users')->getAllUsers(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/members/title/edit/{id}
     */
    public function editAction(Request $request,$id){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2 || !$id) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $user = $bdd->getRepository('DejaVuBundle:Users')->getUserByIdAdmin($id);

        if (!$user) {
            $this->addFlash('alert',
                $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                    array('message' => $this->get('translator')->trans('label.user_not_exist'), 'class' => "erreur")
                )
            );
            return $this->redirectToRoute('deja_vu_admin_management_members');
        }

        if (($user->getId() == $session->get('user')['id']) || ($user->getTitle()->getId() <= $session->get('user')['title']['id']))
            return $this->redirectToRoute('deja_vu_admin_management_members');

        $titles = $bdd->getRepository('DejaVuBundle:Titles')->getTitles($session->get('user')['title']['id']);

        if(!$titles) return $this->redirectToRoute('deja_vu_admin_management_members');

        $form = $this->createFormBuilder()
            ->add('title', ChoiceType::class, array(
                'label'         => $this->get('translator')->trans('label.title'),
                'choices'       => $titles,
                'choice_label'  => function(Titles $titles) {
                    return $titles->getName();
                },
                'data'          => $user->getTitle(), // Valeur par défault
            ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.modify'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $post = $form->getData();
            $url = $this->get('app.log')->urlForLog(
                $this->get('router')->generate('deja_vu_user', array(
                    'name'    => $user->getPseudo(),
                )),
                $user->getPseudo()
            );
            $title = $user->getTitle()->getName();
            try {
                $user->setTitle($bdd->getRepository('DejaVuBundle:Titles')->find($post['title']));
                $bdd->flush();

                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.user_title_updated'), 'class' => "ok")
                    )
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_user_title_updated',array(
                    '%user%'        => $url,
                    '%title%'       => $title,
                    '%new_title%'   => $post['title']->getName(),
                ))
                );

                return $this->redirectToRoute('deja_vu_admin_management_members');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.user_title_not_updated'), 'class' => "erreur")
                    )
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_user_title_updated',array(
                    '%user%'        => $url,
                    '%title%'       => $title,
                    '%new_title%'   => $post['title']->getName(),
                ))
                );
            }
        }

        return $this->render('DejaVuBundle:Default:admin/title_edit.html.twig', array(
            'user'              => $user,
            'form'              => $form->createView(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/members/id/bann/{id}
     */
    public function bannAction(Request $request,$id){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2 || !$id) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $user = $bdd->getRepository('DejaVuBundle:Users')->getUserByIdAdmin($id);
        if (!$user
            || $user->getId() == $session->get('user')['id']
            || $user->getTitle()->getId() <= $session->get('user')['title']['id']
            || $user->getBanned()
        )
            return $this->redirectToRoute('deja_vu_admin_management_members');

        $form = $this->createFormBuilder()
            ->add('message', TextareaType::class, array(
                'constraints'   => array(
                    new NotBlank(array('message' => $this->get('translator')->trans('label.message_empty'))),
                    new Length(array('min' => 10, 'minMessage' => $this->get('translator')->trans('label.message_short')))
                ),
                'label'         => $this->get('translator')->trans('label.message'),
                'attr'          => array(
                    'rows'      => '5',
                    'cols'      => '50'
                )

            ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.bann_id'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            $url = $this->get('app.log')->urlForLog(
                $this->get('router')->generate('deja_vu_user', array(
                    'name'    => $user->getPseudo(),
                )),
                $user->getPseudo()
            );

            try {
                $post = $form->getData();
                $user->setBanned($post['message']);
                $bdd->flush();

                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.user_id_banned'),
                            'class'         => "ok",
                        )
                    )
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_user_id_banned',array(
                    '%user%'        => $url
                ))
                );

                return $this->redirectToRoute('deja_vu_admin_management_members');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.user_id_not_banned'), 'class' => "erreur")
                    )
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_user_id_not_banned',array(
                    '%user%'        => $url
                ))
                );
            }
        }

        return $this->render('DejaVuBundle:Default:admin/bann_id.html.twig', array(
            'user'             => $user,
            'form'              => $form->createView(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/members/id/unbann/{id}
     */
    public function unbannAction(Request $request,$id){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2 || !$id) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $user = $bdd->getRepository('DejaVuBundle:Users')->getUserByIdAdmin($id);
        if (!$user
            || $user->getId() == $session->get('user')['id']
            || $user->getTitle()->getId() <= $session->get('user')['title']['id']
            || !$user->getBanned()
        )
            return $this->redirectToRoute('deja_vu_admin_management_members');

        $form = $this->createFormBuilder()
            ->add('message', TextareaType::class, array(
                'constraints'   => array(
                    new NotBlank(array('message' => $this->get('translator')->trans('label.message_empty'))),
                    new Length(array('min' => 10, 'minMessage' => $this->get('translator')->trans('label.message_short')))
                ),
                'label'         => $this->get('translator')->trans('label.message'),
                'attr'          => array(
                    'rows'      => '5',
                    'cols'      => '50',
                    'readonly'  => true
                ),
                'data'          => $user->getBanned(),

            ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.unbann_id'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            $url = $this->get('app.log')->urlForLog(
                $this->get('router')->generate('deja_vu_user', array(
                    'name'    => $user->getPseudo(),
                )),
                $user->getPseudo()
            );

            try {
                $user->setBanned(null);
                $bdd->flush();

                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.user_id_unbanned'),
                            'class'         => "ok",
                        )
                    )
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_user_id_unbanned',array(
                    '%user%'        => $url
                ))
                );

                return $this->redirectToRoute('deja_vu_admin_management_members');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.user_id_not_unbanned'), 'class' => "erreur")
                    )
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_user_id_not_unbanned',array(
                    '%user%'        => $url
                ))
                );
            }
        }

        return $this->render('DejaVuBundle:Default:admin/bann_id.html.twig', array(
            'user'             => $user,
            'form'              => $form->createView(),
        ));
    } // GOOD

}

