<?php

namespace DejaVuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsController extends Controller
{
    /*
     * Route nomdusite.fr/news
     */
    public function newsAction()
    {
        $manager = $this->getDoctrine()->getManager();

        return $this->render('DejaVuBundle:Default:news.html.twig', array(
            'news_list' => $manager->getRepository('DejaVuBundle:News')->getLastedNews(),
            'news_list2' => $manager->getRepository('DejaVuBundle:News')->getLastedNews2(),
            'users' => $manager->getRepository('DejaVuBundle:Users')->findAll(),
        ));
    } // todo

    /*
     * Route nomdusite.fr/news/{id}
     */
    public function viewnewsAction($id = -1)
    {
        $manager = $this->getDoctrine()->getManager();
        $news = $manager->getRepository('DejaVuBundle:News')->getNewsById($id);

        if (!$news) return $this->redirectToRoute('deja_vu_news');

        return $this->render('DejaVuBundle:Default:news.html.twig', array(
            'id' => $id,
            'news_list' => array($news),
            'com_list' => $manager->getRepository('DejaVuBundle:Comments')->getLastedCommentsForNews($id)
        ));
    } // todo

}

