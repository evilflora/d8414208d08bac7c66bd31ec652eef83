<?php

namespace DejaVuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AccountController extends Controller
{
    /*
     * Route nomdusite.fr/account
     */
    public function accountAction(Request $request) {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id']) return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:account.html.twig', array(
            'user'  => $bdd->getRepository('DejaVuBundle:Users')->find($session->get('user')['id']),
            'stats' => $bdd->getRepository('DejaVuBundle:Episodes')->getUserStats($session->get('user')['id']),
        ));
    } // todo Verifier

}

