<?php

namespace DejaVuBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use DejaVuBundle\Entity\LogSite;

use Doctrine\ORM\EntityManager;


class LogController
{
    protected $bdd;

    public function __construct(EntityManager $bdd)
    {
      $this->bdd = $bdd;
    }

    public function addLog(Request $request, $message, $user = null)
    {
        $session    = $request->getSession();
        $log        = new LogSite();
        $user       = ($session->get('user')['id']?$session->get('user')['id']:$user);
        $log->setUser( $this->bdd->getRepository('DejaVuBundle:Users')->find($user))
            ->setIp($request->getClientIp())
            ->setSummary($message)
        ;
        $this->bdd->persist($log);
        $this->bdd->flush();
    }

    public function urlForLog($href, $name) {
        return '<a href="'.$href.'" target="_blank">'.$name.'</a>';
    }
}
