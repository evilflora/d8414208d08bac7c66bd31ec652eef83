<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\Users;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterController extends Controller
{
    /*
     * Route nomdusite.fr/register
     */
    public function registerAction(Request $request)
    {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if($session->get('user')['id']) return $this->redirectToRoute('deja_vu_home');

        $form = $this
            ->createFormBuilder()
            ->add('pseudo', TextType::class, array(
                'constraints' => array(
                    new NotBlank(array('message' => "Indiquez votre pseudo")),
                    new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.pseudo_short'))),
                    new Length(array('max' => 16, 'maxMessage' => $this->get('translator')->trans('label.pseudo_long')))
                )
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux mots de passes ne correspondent pas.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array(
                    'constraints' => array(
                        new NotBlank(array('message' => "Indiquez votre mot de passe")),
                        new Length(array('min' => 8, 'minMessage' => $this->get('translator')->trans('label.password_short'))),
                        new Length(array('max' => 128, 'maxMessage' => $this->get('translator')->trans('label.password_short')))
                    )
                ),
                'second_options' => array(
                    'constraints' => array(
                        new NotBlank(array('message' => "Repatez votre pseudo")),
                        new Length(array('min' => 8, 'minMessage' => $this->get('translator')->trans('label.password_short'))),
                        new Length(array('max' => 128, 'maxMessage' => $this->get('translator')->trans('label.password_short')))
                    )
                )
            ))
            ->add('email', EmailType::class, array(
                'constraints' => array(
                    new NotBlank(array('message' => "Indiquez votre email")),
                    new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.email_short'))),
                    new Length(array('max' => 32, 'maxMessage' => $this->get('translator')->trans('label.email_short')))
                )
            ))
            ->add('save', SubmitType::class)
            ->add('recaptcha', EWZRecaptchaType::class, array(
                'attr' => array(
                    'options' => array(
                        'theme' => 'light',
                        'type'  => 'image',
                        'size'  => 'normal',
                        'defer' => true,
                        'async' => true
                    )
                ),
                'mapped'      => false,
                'constraints' => array(
                    new RecaptchaTrue()
                )
            ))
            ->getForm();

        $form->handleRequest($request);
        $register = $form->getData();

        $get_user = $bdd->getRepository('DejaVuBundle:Users')->findOneBy(array('pseudo' => $register['pseudo']));

        if ($get_user) {
            $form->get('pseudo')->addError(new FormError($this->get('translator')->trans('label.user_already_registered')));
        }

        $get_email = $bdd->getRepository('DejaVuBundle:Users')->findOneBy(array('email' => $register['email']));

        if ($get_email) {
            $form->get('email')->addError(new FormError($this->get('translator')->trans('label.email_already_registered')));
        }

        if ($form->isValid() && $form->isSubmitted()) {

            $title  = $bdd->getRepository('DejaVuBundle:Titles')->find(3);
            $ip     = $bdd->getRepository('DejaVuBundle:BannedIp')->findOneBy(array('ip' => $request->getClientIp()));

            $user = new Users();
            $user->setPseudo($register['pseudo'])
                ->setEmail($register['email'])
                ->setTitle($title)
                ->setPassword(hash('sha256',$register['password']))
                ->setIp($ip);
            $bdd->persist($user);
            $bdd->flush();

            $this->addFlash('alert',
                $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                    array('message' => $this->get('translator')->trans('label.user_registered'), 'class' => "ok")
                )
            );
            $this->get('app.log')->addLog(
                $request,
                $this->get('translator')->trans('label.log_user_registered', array(
                    '%user%'    =>$user->getPseudo()
                )),
                $user->getId()
            );
        }

        return $this->render('DejaVuBundle:Default:register.html.twig', array(
            'form' => $form->createView()
        ));
    } // GOOD

}
