<?php

namespace DejaVuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /*
     * Route nomdusite.fr/user/{name}
     */
    public function viewAction(Request $request, $name)
    {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        $user = $bdd->getRepository('DejaVuBundle:Users')->findOneBy(array('pseudo' => $name));
        if (!$user) {
            $this->addFlash('alert',
                $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                        'message' => $this->get('translator')->trans('label.user_not_exist'),
                        'class' => "erreur",
                    )
                )
            );

            return $this->redirectToRoute('deja_vu_home');
        }

        return $this->render('DejaVuBundle:Default:user.html.twig', array(
            'user' => $user,
            'stats' => $bdd->getRepository('DejaVuBundle:Episodes')->getUserStats($session->get('user')['id']),
        ));
    } // GOOD
}
