<?php

namespace DejaVuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CatalogController extends Controller
{
    /*
     * Route nomdusite.fr/list
     */
    public function catalogAction(Request $request){

        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id']) // Si on est déjà connecté on redirige vers les news
            $series = $bdd->getRepository('DejaVuBundle:Series')->getAllSeries();
        else
            $series = $bdd->getRepository('DejaVuBundle:Series')->getAllUserSeries($session->get('user')['id']);

        return $this->render('DejaVuBundle:Default:catalog.html.twig', array(
            'type_media' => $bdd->getRepository('DejaVuBundle:Type')->findAll(),
            'kind'       => $bdd->getRepository('DejaVuBundle:Kind')->findAll(),
            'stats'      => $bdd->getRepository('DejaVuBundle:Series')->getStats(),
            'series'     => $series,
        ));
    } // todo Verifier

    /*
     * Route nomdusite.fr/list/{id}
     */
    public function viewcatalogAction(Request $request,$id = -1){

        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        $series = $bdd->getRepository('DejaVuBundle:Series')->getUserSerie($id,$session->get('user')['id']);
        $seasons = $bdd->getRepository('DejaVuBundle:Seasons')->getUserSeasons($id,$session->get('user')['id']);
        $user_episodes = $bdd->getRepository('DejaVuBundle:Episodes')->getUserEpisodes($id,$session->get('user')['id']);

        $result = array();
        $l = 0;

        foreach ($seasons as $season) {
            for($j = 0; $j < $season['nb_episode']; $j++) {
                $check='';
                $etat=0;
                if(isset($user_episodes)) {
                    $episode = isset($user_episodes[$l]['episode'])?$user_episodes[$l]['episode']:$j;;

                    if($episode == $j+1 && $season['season'] == $user_episodes[$l]['season']) {
                        $check='checked';
                        $etat=1;
                    }
                }

                array_push($result,array('id' => $j+1,'season' => $season['season']+1,'state' => $etat,'checked' => $check));

                if($episode == $j+1 && $season['season'] == $user_episodes[$l]['season']) {
                    $l++;
                }
            }
        }

        if (!$series) return $this->redirectToRoute("deja_vu_catalog");

        $session->set('seasons', $seasons);

        return $this->render('DejaVuBundle:Default:viewcatalog.html.twig', array(
            'series'            => $series,
            'seasons'           => $seasons,
            'user_episodes'     => $user_episodes,
            'result'            => $result,
        ));
    } // todo Verifier

}

