<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\Type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminCategoryController extends Controller
{
    /*
     * Route nomdusite.fr/admin/category
     */
    public function viewAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:admin/category.html.twig', array(
            'categories'       => $bdd->getRepository('DejaVuBundle:Type')->findAll(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/category/add
     */
    public function addAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');


        $form = $this->createFormBuilder() // Form
        ->add('name', TextType::class, array(
            'constraints'   => array(
                new NotBlank(array('message' => $this->get('translator')->trans('label.cat_empty'))),
                new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.cat_short'))),
                new Length(array('max' => 32, 'maxMessage' => $this->get('translator')->trans('label.cat_long')))
            ),
            'label'         => $this->get('translator')->trans('label.name'),
            'attr'          => array(
                'size'      => '40',
            ),
        ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.add'),
            ))
            ->getForm();

        $form->handleRequest($request);
        $post = $form->getData();

        if ($form->isValid() && $form->isSubmitted()) {
            if($cat = $bdd->getRepository('DejaVuBundle:Type')->findOneBy(array('name' => $post['name']))) { // Already exist
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.cat_already_exist'),
                            'class'         => "erreur",
                        )
                    )
                );

                $url = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_category_edit', array(
                        'id'    => $cat->getId(),
                    )),
                    $cat->getName()
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_already_exist',array(
                    '%cat%'        => $url
                ))
                );
            } else {
                try {
                    $category = new Type();
                    $category->setName($post['name']);
                    $bdd->persist($category);
                    $bdd->flush();

                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                                'message'       => $this->get('translator')->trans('label.cat_added'),
                                'class'         => "ok",
                            )
                        )
                    ); // OK

                    $url = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_category_edit', array(
                            'id'    => $category->getId(),
                        )),
                        $category->getname()
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_added',array(
                        '%cat%'        => $url
                    ))
                    );

                    return $this->redirectToRoute('deja_vu_admin_category');
                } catch (Exception $e) {
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                            array('message' => $this->get('translator')->trans('label.cat_not_added'), 'class' => "erreur")
                        )
                    ); // Erreur

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_not_added',array(
                        '%cat%'        => $post['name']
                    ))
                    );
                }
            }
        }

        return $this->render('DejaVuBundle:Default:admin/category_add.html.twig', array(
            'form'              => $form->createView(),
        ));
    }

    /*
     * Route nomdusite.fr/admin/category/edit/{id}
     */
    public function editAction(Request $request,$id){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        if(!$category = $bdd->getRepository('DejaVuBundle:Type')->find($id)) {
            $this->addFlash('alert',
                $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                        'message'       => $this->get('translator')->trans('label.cat_not_exist'),
                        'class'         => "erreur",
                    )
                )
            );
            return $this->redirectToRoute('deja_vu_admin_category');
        }

        $form = $this->createFormBuilder() // Form
        ->add('name', TextType::class, array(
            'label'         => $this->get('translator')->trans('label.name'),
            'attr'          => array(
                'size'      => '40',
            ),
            'data'          => $category->getName(),
        ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.modify'),
            ))
            ->getForm();

        $form->handleRequest($request);
        $post = $form->getData();


        if ($form->isValid() && $form->isSubmitted()) {
            $cat = $bdd->getRepository('DejaVuBundle:Type')->findOneBy(array('name' => $post['name'])) && $post['name'] != $category->getName();
            if($cat) { // Already exist
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.cat_already_exist'),
                            'class'         => "erreur",
                        )
                    )
                );
                $url_before = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_category_edit', array(
                        'id'    => $category->getId(),
                    )),
                    $category->getName()
                );
                $url_after = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_category_edit', array(
                        'id'    => $cat->getId(),
                    )),
                    $cat->getName()
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_edit_already_exist',array(
                    '%cat%'         => $url_before,
                    '%newcat%'      =>$url_after,
                ))
                );
            } else {
                try {
                    $cat_before = $category->getName();
                    $category->setName($post['name']);
                    $bdd->flush();
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                                'message'       => $this->get('translator')->trans('label.cat_edited'),
                                'class'         => "ok",
                            )
                        )
                    ); // OK

                    $url_after = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_category_edit', array(
                            'id'    => $category->getId(),
                        )),
                        $category->getName()
                    );
                    $url_before = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_category_edit', array(
                            'id'    => $category->getId(),
                        )),
                        $cat_before
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_modified',array(
                        '%cat%'         => $url_before,
                        '%newcat%'      => $url_after,
                    ))
                    );

                    return $this->redirectToRoute('deja_vu_admin_category');
                } catch (Exception $e) {
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                            array('message' => $this->get('translator')->trans('label.cat_not_edited'), 'class' => "erreur")
                        )
                    ); // Erreur

                    $url = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_category_edit', array(
                            'id'    => $category->getId(),
                        )),
                        $category->getName()
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_not_modified',array(
                        '%cat%'        => $url
                    ))
                    );
                }
            }
        }

        return $this->render('DejaVuBundle:Default:admin/category_edit.html.twig',array(
            'form'              => $form->createView(),
        ));
    }

    /*
     * Route nomdusite.fr/admin/category/delete/{id}
     */
    public function deleteAction(Request $request, $id) {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $category = $bdd->getRepository('DejaVuBundle:Type')->find($id);

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.delete'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            try {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.cat_deleted'), 'class' => "ok")
                    )
                );

                $category_log = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_category_edit', array(
                        'id'    => $category->getId(),
                    )),
                    $category->getName()
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_deleted',array('%cat%' => $category_log)));

                $bdd->remove($category);
                $bdd->flush();

                return $this->redirectToRoute('deja_vu_admin_category');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.cat_not_deleted'), 'class' => "erreur")
                    )
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_not_deleted',array('%cat%' => $category->getName())));
            }
        } // todo

        return $this->render('DejaVuBundle:Default:admin/category_delete.html.twig', array(
            'form'              => $form->createView(),
            'title'             => $category->getName(),
            'id'                => $id,
        ));
    }
}
