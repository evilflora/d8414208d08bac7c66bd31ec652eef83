<?php

namespace DejaVuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminLogsController extends Controller
{
    /*
     * Route nomdusite.fr/admin/logs
     */
    public function viewAction(Request $request) {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 1) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:admin/logs.html.twig',array(
            'logs'          => $bdd->getRepository('DejaVuBundle:LogSite')->findAll(),
        ));
    } // GOOD

}

