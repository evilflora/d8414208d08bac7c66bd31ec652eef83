<?php

namespace DejaVuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class LoginController extends Controller
{
    /*
     * Route nomdusite.fr/login
     */
    public function loginAction(Request $request) // GOOD
    {
        $session = $request->getSession();

        // Check if connected
        if($session->get('user')['id']) return $this->redirectToRoute('deja_vu_home');

        // Create register form
        $form = $this->createFormBuilder()
            ->add('pseudo', TextType::class, array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.pseudo_short'))),
                    new Length(array('max' => 16, 'maxMessage' => $this->get('translator')->trans('label.pseudo_long')))

                )
            ))
            ->add('password', PasswordType::class, array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 8, 'minMessage' => $this->get('translator')->trans('label.password_short'))),
                    new Length(array('max' => 128, 'maxMessage' => $this->get('translator')->trans('label.password_short')))
                )
            ))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        // Form correct and posted
        if ($form->isValid() && $form->isSubmitted()) {

            $post = $form->getData();

            $bdd = $this->getDoctrine()->getManager();
            $get_user = $bdd->getRepository('DejaVuBundle:Users')->findOneBy(array('pseudo' => $post['pseudo']));

            if ($get_user) {
                if ($get_user->getPassword() == hash('sha256',$post['password'])) {

                    // Sav User
                    $session->set('user', array(
                        'id' => $get_user->getId(),
                        'pseudo' => $get_user->getPseudo(),
                        'registrationDate'=> $get_user->getRegistrationDate(),
                        'email' => $get_user->getEmail(),
                        'title' => array(
                            'id' => $get_user->getTitle()->getId(),
                            'name' => $get_user->getTitle()->getName(),
                        ),
                    ));

                    // Save IP
                    $ip = $bdd->getRepository('DejaVuBundle:BannedIp')->findOneBy(array('ip' => $request->getClientIp()));
                    $get_user->setIp($ip);
                    $bdd->flush();

                    // Log
                    $this->get('app.log')->addLog(
                        $request,
                        $this->get('translator')->trans('label.log_user_connected'),
                        $get_user->getId()
                    );

                    return $this->redirectToRoute('deja_vu_home');

                } else {
                    $form->get('password')->addError(new FormError($this->get('translator')->trans('label.wrong_password')));

                    $this->get('app.log')->addLog(
                        $request,
                        $this->get('translator')->trans('label.log_user_wrong_password'),
                        $get_user->getId()
                    );

                }
            } else {
                $form->get('pseudo')->addError(new FormError($this->get('translator')->trans('label.user_not_exist')));
            }
        }

        return $this->render('DejaVuBundle:Default:login.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /*
     * Route nomdusite.fr/logout
     */
    public function logoutAction(Request $request)
    {
        $session = $request->getSession();

        $session->remove('user');

        return $this->redirectToRoute('deja_vu_home');
    } // GOOD
}
