<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\Kind;
use DejaVuBundle\Entity\Licensee;
use DejaVuBundle\Entity\Type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminContentController extends Controller
{
    /*
     * Route nomdusite.fr/admin/catalog
     */
    public function viewAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:admin/catalog.html.twig', array(
            'series'             => $bdd->getRepository('DejaVuBundle:Series')->getAllSeriesAdmin(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/catalog/add
     */
    public function addAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();
        $years = null; // Yeah its seem useless but its necessary

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $categories = $bdd->getRepository('DejaVuBundle:Type')->findAll();
        $kind = $bdd->getRepository('DejaVuBundle:Kind')->findAll();
        $licensee = $bdd->getRepository('DejaVuBundle:Licensee')->findAll();
        for( $i = date("Y"); $i >= 1860; $i--) {
            $years[$i] = $i;
        }

        $form = $this->createFormBuilder() // Form
        ->add('title', TextType::class, array(
            'constraints'   => array(
                new NotBlank(array('message' => $this->get('translator')->trans('label.cat_empty'))),
                new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.cat_short'))),
                new Length(array('max' => 64, 'maxMessage' => $this->get('translator')->trans('label.cat_long')))
            ),
            'label'         => $this->get('translator')->trans('label.title'),
        ))
        ->add('alternativ_title', TextType::class, array(
            'constraints'   => array(
                new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.cat_short'))),
                new Length(array('max' => 64, 'maxMessage' => $this->get('translator')->trans('label.cat_long')))
            ),
            'required' => false,
            'label'         => $this->get('translator')->trans('label.alternativ_title'),
        ))
        ->add('authors', TextType::class, array(
            'constraints'   => array(
                new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.cat_short'))),
                new Length(array('max' => 128, 'maxMessage' => $this->get('translator')->trans('label.cat_long')))
            ),
            'label'         => $this->get('translator')->trans('label.authors'),
        ))
        ->add('description', TextareaType::class, array(
            'constraints'   => array(
                new Length(array('min' => 20, 'minMessage' => $this->get('translator')->trans('label.cat_short'))),
            ),
            'label'         => $this->get('translator')->trans('label.description'),
        ))
        ->add('category', ChoiceType::class, array(
            'label'         => $this->get('translator')->trans('label.category'),
            'choices'       => $categories,
            'choice_label'  => function(Type $categories) {
                return $categories->getName();
            },
        ))
        ->add('kind', ChoiceType::class, array(
            'label'         => $this->get('translator')->trans('label.kind'),
            'choices'       => $kind,
            'required'      => false,
            'choice_label'  => function(Kind $kind) {
                return $kind->getName();
            },
            'multiple' => true,
        ))
        ->add('license_name', ChoiceType::class, array(
            'label'         => $this->get('translator')->trans('label.licensee'),
            'choices'       => $licensee,
            'placeholder'   => '',
            'required'      => false,
            'choice_label'  => function(Licensee $licensee) {
                return $licensee->getName();
            },
        ))
        ->add('episode_per_season', CollectionType::class, array(
            'allow_add'     => true,
            'allow_delete'  => true,
            'entry_type'    => TextType::class,
            'label'         => false,
            'entry_options' => array(
                'constraints'   => array(
                    new Length(array('min' => 1, 'minMessage' => $this->get('translator')->trans('label.cat_short'))),
                    new Length(array('max' => 4, 'maxMessage' => $this->get('translator')->trans('label.cat_long')))
                ),
                'label'         => false,
                'attr'          => array('min' => '1','step' => '1'),
            ),
        ))
        ->add('year_per_season', CollectionType::class, array(
            'allow_add'     => true,
            'allow_delete'  => true,
            'prototype'     => true,
            'entry_type'    => ChoiceType::class,
            'label'         => false,
            'entry_options' => array(
                'label'         => false,
                'choices'       => $years,
            ),
        ))
        ->add('finished_per_season', CollectionType::class, array(
            'allow_add'     => true,
            'allow_delete'  => true,
            'prototype'     => true,
            'entry_type'    => CheckboxType::class,
            'label'         => false,
            'entry_options' => array(
                'required'      => false,
                'label'         => false,
            ),
        ))
        ->add('save', SubmitType::class, array(
            'label'         => $this->get('translator')->trans('label.add'),
        ))
        ->getForm();

        $form->handleRequest($request);
        $post = $form->getData();

        $nb_season = count($post['episode_per_season']);
        for ($i = 0; $i < $nb_season; $i++){
            $post['finished_per_season'][$i] = isset($post['finished_per_season'][$i]);
        }

        if ($form->isValid() && $form->isSubmitted()) {
            /*
            try {
                $serie = new Series();
                $serie->setTitle($post['title']);
                $serie->setAlternativTitle($post['alternativ_title']);
                $serie->setAuthors($post['authors']);
                $serie->setDescription($post['description']);
                $serie->setType($post['category']);
                foreach ($post['kind'] as $kind) {
                    $serie->addKind($kind);
                }
                $serie->setLicense($post['license_name']);
                $bdd->persist($serie);
                for ($i = 0; $i < count($post['episode_per_season']); $i++) {
                    $season = new Seasons();
                    $season->setSeries($serie);
                    $season->setSeason($i);
                    $season->setYear($post['year_per_season'][$i]);
                    $season->setIsFinish($post['finished_per_season'][$i]);
                    $bdd->persist($season);
                    for ($j = 0; $j < count($post['episode_per_season']); $j++) {
                        $episode = new Episodes();
                        $episode->setSeason($i);
                        $episode->setEpisode($j + 1);
                        $bdd->persist($episode);
                    }
                }


                $bdd->persist($serie);
                $bdd->flush();

                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.cat_added'),
                            'class'         => "ok",
                        )
                    )
                ); // OK

                $url = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_category_edit', array(
                        'id'    => $serie->getId(),
                    )),
                    $serie->getname()
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_added',array(
                    '%cat%'        => $url
                )));

                return $this->redirectToRoute('deja_vu_admin_category');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.cat_not_added'), 'class' => "erreur")
                    )
                ); // Erreur

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_cat_not_added',array(
                    '%cat%'        => $post['name']
                )));
            }*/
        } // todo

        return $this->render('DejaVuBundle:Default:admin/catalog_add.html.twig',array(
            'categories'        => $bdd->getRepository('DejaVuBundle:Type')->findAll(),
            'kinds'             => $bdd->getRepository('DejaVuBundle:Kind')->findAll(),
            'form'              => $form->createView(),
            'data_form'         => $form,
        ));
    } // todo

    /*
     * Route nomdusite.fr/admin/catalog/edit/{id}
     */
    public function editAction(Request $request,$id){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $serie = $bdd->getRepository('DejaVuBundle:Series')->getSerie($id);
        $serie['kind'] = explode(',',$serie['kind']);

        $kinds = $bdd->getRepository('DejaVuBundle:Type')->getAllKinds();

        foreach($kinds as $i=>$kind) {
            $selected = ((isset($serie['kind'][$i]) && $serie['kind'][$i] == $kind['name']) ? 'selected' : false);
            $kinds[$i]['selected'] = $selected;
        }
        return $this->render('DejaVuBundle:Default:admin/catalog_edit.html.twig', array(
            'serie'             => $serie,
            'seasons'           => $bdd->getRepository('DejaVuBundle:Seasons')->getSeasons($id),
            'categories'        => $bdd->getRepository('DejaVuBundle:Type')->findAll(),
            'kinds'             => $kinds,
        ));
    } // todo

    public function addSeasonAction(Request $request) {
        if($request->isXmlHttpRequest()) {
            return $this->render('DejaVuBundle:Default:admin/_season.html.twig', array(
                'index'         => $request->get('index'),
                'episode'       => $request->get('episode'),
                'year'          => $request->get('year'),
                'finish'        => $request->get('finish'),
                'is_ajax_call'  => true,
            ));
        }
    }
}
