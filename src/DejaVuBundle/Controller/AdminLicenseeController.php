<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\Licensee;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminLicenseeController extends Controller
{
    /*
     * Route nomdusite.fr/admin/licensee
     */
    public function viewAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:admin/licensee.html.twig',array(
            'licensees'         => $bdd->getRepository('DejaVuBundle:Licensee')->findAll(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/licensee/add
     */
    public function addAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');


        $form = $this->createFormBuilder() // Form
        ->add('name', TextType::class, array(
            'constraints'   => array(
                new NotBlank(array('message' => $this->get('translator')->trans('label.licensee_empty'))),
                new Length(array('min' => 3, 'minMessage' => $this->get('translator')->trans('label.licensee_short'))),
                new Length(array('max' => 32, 'maxMessage' => $this->get('translator')->trans('label.licensee_long')))
            ),
            'label'         => $this->get('translator')->trans('label.name'),
            'attr'          => array(
                'size'      => '40',
            ),
        ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.add'),
            ))
            ->getForm();

        $form->handleRequest($request);
        $post = $form->getData();


        if ($form->isValid() && $form->isSubmitted()) {
            $licensee = $bdd->getRepository('DejaVuBundle:Licensee')->findOneBy(array('name' => $post['name']));
            if($licensee) { // Already exist
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.licensee_already_exist'),
                            'class'         => "erreur",
                        )
                    )
                );
                $url = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_licensee_edit', array(
                        'id'    => $licensee->getId(),
                    )),
                    $licensee->getName()
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_licensee_already_exist',array(
                    '%licensee%'         => $url,
                ))
                );
            } else {
                try {
                    $licensee = new Licensee();
                    $licensee->setName($post['name']);
                    $bdd->persist($licensee);
                    $bdd->flush();
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                                'message'       => $this->get('translator')->trans('label.licensee_added'),
                                'class'         => "ok",
                            )
                        )
                    ); // OK

                    $url = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_licensee_edit', array(
                            'id'    => $licensee->getId(),
                        )),
                        $licensee->getname()
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_licensee_added',array(
                        '%licensee%'        => $url
                    ))
                    );

                    return $this->redirectToRoute('deja_vu_admin_licensee');
                } catch (Exception $e) {
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                            array('message' => $this->get('translator')->trans('label.licensee_not_added'), 'class' => "erreur")
                        )
                    ); // Erreur

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_licensee_not_added',array(
                        '%licensee%'        => $post['name']
                    ))
                    );
                }
            }
        }

        return $this->render('DejaVuBundle:Default:admin/licensee_add.html.twig', array(
            'form'              => $form->createView(),
        ));
    }

    /*
     * Route nomdusite.fr/admin/licensee/edit/{id}
     */
    public function editAction(Request $request,$id) {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        if(!$licensee = $bdd->getRepository('DejaVuBundle:Licensee')->find($id)) {
            $this->addFlash('alert',
                $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                        'message'       => $this->get('translator')->trans('label.licensee_not_exist'),
                        'class'         => "erreur",
                    )
                )
            );
        }

        $form = $this->createFormBuilder() // Form
        ->add('name', TextType::class, array(
            'constraints'   => array(
                new NotBlank(array('message' => $this->get('translator')->trans('label.licensee_empty'))),
                new Length(array('min' => 3, 'minMessage' => $this->get('translator')->trans('label.licensee_short'))),
                new Length(array('max' => 32, 'maxMessage' => $this->get('translator')->trans('label.licensee_long')))
            ),
            'label'         => $this->get('translator')->trans('label.name'),
            'attr'          => array(
                'size'      => '40',
            ),
            'data'          => $licensee->getName(),
        ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.modify'),
            ))
            ->getForm();

        $form->handleRequest($request);
        $post = $form->getData();

        if ($form->isValid() && $form->isSubmitted()) {
            $licensee2 = $bdd->getRepository('DejaVuBundle:Licensee')->findOneBy(array('name' => $post['name'])) && $post['name'] != $licensee->getName();
            if($licensee2) { // Already exist
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.licensee_already_exist'),
                            'class'         => "erreur",
                        )
                    )
                );
                $url_before = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_licensee_edit', array(
                        'id'    => $licensee->getId(),
                    )),
                    $licensee->getName()
                );
                $url_after = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_licensee_edit', array(
                        'id'    => $licensee2->getId(),
                    )),
                    $licensee2->getName()
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_licensee_edit_already_exist',array(
                    '%licensee%'         => $url_before,
                    '%newlicensee%'      => $url_after,
                ))
                );
            } else {
                try {
                    $licensee_before = $licensee->getName();
                    $licensee->setName($post['name']);
                    $bdd->flush();
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                                'message'       => $this->get('translator')->trans('label.licensee_edited'),
                                'class'         => "ok",
                            )
                        )
                    ); // OK


                    $url_after = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_licensee_edit', array(
                            'id'    => $licensee->getId(),
                        )),
                        $licensee->getName()
                    );
                    $url_before = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_licensee_edit', array(
                            'id'    => $licensee->getId(),
                        )),
                        $licensee_before
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_licensee_modified',array(
                        '%licensee%'         => $url_before,
                        '%newlicensee%'      => $url_after,
                    ))
                    );

                    return $this->redirectToRoute('deja_vu_admin_licensee');
                } catch (Exception $e) {
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                            array('message' => $this->get('translator')->trans('label.licensee_not_edited'), 'class' => "erreur")
                        )
                    ); // Erreur

                    $url = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_licensee_edit', array(
                            'id'    => $licensee->getId(),
                        )),
                        $licensee->getName()
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_licensee_not_modified',array(
                        '%licensee%'        => $url
                    ))
                    );
                }
            }
        }

        return $this->render('DejaVuBundle:Default:admin/licensee_edit.html.twig',array(
            'form'              => $form->createView(),
        ));
    }

    /*
     * Route nomdusite.fr/admin/licensee/delete/{id} // todo
     */
    public function deleteAction(Request $request,$id) {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $licensee = $bdd->getRepository('DejaVuBundle:Licensee')->find($id);

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.delete'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            try {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.licensee_deleted'), 'class' => "ok")
                    )
                );

                $licensee_log = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_licensee_edit', array(
                        'id'    => $licensee->getId(),
                    )),
                    $licensee->getName()
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_licensee_deleted',array('%licensee%' => $licensee_log)));

                $bdd->remove($licensee);
                $bdd->flush();

                return $this->redirectToRoute('deja_vu_admin_licensee');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.licensee_not_deleted'), 'class' => "erreur")
                    )
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_licensee_not_deleted',array('%licensee%' => $licensee->getName())) . "|" . $e->getMessage());
            }
        } // todo

        return $this->render('DejaVuBundle:Default:admin/licensee_delete.html.twig', array(
            'form'              => $form->createView(),
            'title'             => $licensee->getName(),
            'id'                => $id,
        ));
    }
}
