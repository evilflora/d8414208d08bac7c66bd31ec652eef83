<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\Kind;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminKindController extends Controller
{
    /*
     * Route nomdusite.fr/admin/kind
     */
    public function viewAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:admin/kind.html.twig',array(
            'kinds'         => $bdd->getRepository('DejaVuBundle:Kind')->findAll(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/kind/add
     */
    public function addAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');


        $form = $this->createFormBuilder() // Form
        ->add('name', TextType::class, array(
            'constraints'   => array(
                new NotBlank(array('message' => $this->get('translator')->trans('label.kind_empty'))),
                new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.kind_short'))),
                new Length(array('max' => 32, 'maxMessage' => $this->get('translator')->trans('label.kind_long')))
            ),
            'label'         => $this->get('translator')->trans('label.name'),
            'attr'          => array(
                'size'      => '40',
            ),
        ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.add'),
            ))
            ->getForm();

        $form->handleRequest($request);
        $post = $form->getData();


        if ($form->isValid() && $form->isSubmitted()) {
            $kind = $bdd->getRepository('DejaVuBundle:Kind')->findOneBy(array('name' => $post['name']));
            if($kind) { // Already exist
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.kind_already_exist'),
                            'class'         => "erreur",
                        )
                    )
                );
                $url = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_kind_edit', array(
                        'id'    => $kind->getId(),
                    )),
                    $kind->getName()
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_kind_already_exist',array(
                    '%kind%'         => $url,
                ))
                );
            } else {
                try {
                    $kind = new Kind();
                    $kind->setName($post['name']);
                    $bdd->persist($kind);
                    $bdd->flush();
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                                'message'       => $this->get('translator')->trans('label.kind_added'),
                                'class'         => "ok",
                            )
                        )
                    ); // OK

                    $url = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_kind_edit', array(
                            'id'    => $kind->getId(),
                        )),
                        $kind->getname()
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_kind_added',array(
                        '%kind%'        => $url
                    ))
                    );

                    return $this->redirectToRoute('deja_vu_admin_kind');
                } catch (Exception $e) {
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                            array('message' => $this->get('translator')->trans('label.kind_not_added'), 'class' => "erreur")
                        )
                    ); // Erreur

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_kind_not_added',array(
                        '%kind%'        => $post['name']
                    ))
                    );
                }
            }
        }

        return $this->render('DejaVuBundle:Default:admin/kind_add.html.twig', array(
            'form'              => $form->createView(),
        ));
    }

    /*
     * Route nomdusite.fr/admin/kind/edit/{id}
     */
    public function editAction(Request $request,$id) {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        if(!$kind = $bdd->getRepository('DejaVuBundle:Kind')->find($id)) {
            $this->addFlash('alert',
                $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                        'message'       => $this->get('translator')->trans('label.kind_not_exist'),
                        'class'         => "erreur",
                    )
                )
            );
        }

        $form = $this->createFormBuilder() // Form
        ->add('name', TextType::class, array(
            'label'         => $this->get('translator')->trans('label.name'),
            'attr'          => array(
                'size'      => '40',
            ),
            'data'          => $kind->getName(),
        ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.modify'),
            ))
            ->getForm();

        $form->handleRequest($request);
        $post = $form->getData();

        if ($form->isValid() && $form->isSubmitted()) {
            $kind2 = $bdd->getRepository('DejaVuBundle:Kind')->findOneBy(array('name' => $post['name'])) && $post['name'] != $kind->getName();
            if($kind2) { // Already exist
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.kind_already_exist'),
                            'class'         => "erreur",
                        )
                    )
                );
                $url_before = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_kind_edit', array(
                        'id'    => $kind->getId(),
                    )),
                    $kind->getName()
                );
                $url_after = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_kind_edit', array(
                        'id'    => $kind2->getId(),
                    )),
                    $kind2->getName()
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_kind_edit_already_exist',array(
                    '%kind%'         => $url_before,
                    '%newkind%'      => $url_after,
                ))
                );
            } else {
                try {
                    $kind_before = $kind->getName();
                    $kind->setName($post['name']);
                    $bdd->flush();
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                                'message'       => $this->get('translator')->trans('label.kind_edited'),
                                'class'         => "ok",
                            )
                        )
                    ); // OK


                    $url_after = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_kind_edit', array(
                            'id'    => $kind->getId(),
                        )),
                        $kind->getName()
                    );
                    $url_before = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_kind_edit', array(
                            'id'    => $kind->getId(),
                        )),
                        $kind_before
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_kind_modified',array(
                        '%kind%'         => $url_before,
                        '%newkind%'      => $url_after,
                    ))
                    );

                    return $this->redirectToRoute('deja_vu_admin_kind');
                } catch (Exception $e) {
                    $this->addFlash('alert',
                        $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                            array('message' => $this->get('translator')->trans('label.kind_not_edited'), 'class' => "erreur")
                        )
                    ); // Erreur

                    $url = $this->get('app.log')->urlForLog(
                        $this->get('router')->generate('deja_vu_admin_kind_edit', array(
                            'id'    => $kind->getId(),
                        )),
                        $kind->getName()
                    );

                    $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_kind_not_modified',array(
                        '%kind%'        => $url
                    ))
                    );
                }
            }
        }

        return $this->render('DejaVuBundle:Default:admin/kind_edit.html.twig',array(
            'form'              => $form->createView(),
        ));
    }

    /*
     * Route nomdusite.fr/admin/kind/delete/{id} // todo
     */
    public function deleteAction(Request $request,$id) {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $kind = $bdd->getRepository('DejaVuBundle:Kind')->find($id);

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.delete'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            try {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.kind_deleted'), 'class' => "ok")
                    )
                );

                $kind_log = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_kind_edit', array(
                        'id'    => $kind->getId(),
                    )),
                    $kind->getName()
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_kind_deleted',array('%kind%' => $kind_log)));

                $bdd->remove($kind);
                $bdd->flush();

                return $this->redirectToRoute('deja_vu_admin_kind');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.kind_not_deleted'), 'class' => "erreur")
                    )
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_kind_not_deleted',array('%kind%' => $kind->getName())));
            }
        } // todo

        return $this->render('DejaVuBundle:Default:admin/kind_delete.html.twig', array(
            'form'              => $form->createView(),
            'title'             => $kind->getName(),
            'id'                => $id,
        ));
    }
}
