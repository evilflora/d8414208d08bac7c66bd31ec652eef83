<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminNewsController extends Controller
{
    /*
     * Route nomdusite.fr/admin/news
     */
    public function viewAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:admin/news.html.twig', array(
            'news_list'         => $bdd->getRepository('DejaVuBundle:News')->getNewsList(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/news/add
     */
    public function addAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        // create a task and give it some dummy data for this example
        $form = $this->createFormBuilder()
            ->add('title', TextType::class, array(
                'constraints'   => array(
                    new NotBlank(array('message' => $this->get('translator')->trans('label.title_empty'))),
                    new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.title_short'))),
                    new Length(array('max' => 40, 'maxMessage' => $this->get('translator')->trans('label.title_long')))
                ),
                'label'         => $this->get('translator')->trans('label.title'),
            ))
            ->add('message', TextareaType::class, array(
                'constraints'   => array(
                    new NotBlank(array('message' => $this->get('translator')->trans('label.message_empty'))),
                    new Length(array('min' => 10, 'minMessage' => $this->get('translator')->trans('label.message_short'))),
                ),
                'required'      => false,
            ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.add'),
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $post = $form->getData();
            try {
                $user = $bdd->getRepository('DejaVuBundle:Users')->find($session->get('user')['id']);
                $news = new News();

                $news->setTitle($post['title'])
                    ->setMessage($post['message'])
                    ->setUser($user);
                $bdd->persist($news);
                $bdd->flush();

                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.news_added'), 'class' => "ok")
                    )
                );

                $news = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_news_edit', array(
                        'id'    => $news->getId(),
                    )),
                    $news->getTitle()
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_news_added',array('%news%' => $news)));

                return $this->redirectToRoute('deja_vu_admin_news');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.news_not_added'), 'class' => "erreur")
                    )
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_news_not_added',array('%news%' => $post['title'])));
            }
        }

        return $this->render('DejaVuBundle:Default:admin/news_add.html.twig', array(
            'form'              => $form->createView(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/news/edit/{id}
     */
    public function editAction(Request $request,$id){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $news = $bdd->getRepository('DejaVuBundle:News')->getNewsByIdAdmin($id);

        if(!$news) {
            $this->addFlash('alert',
                $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                    array('message' => $this->get('translator')->trans('label.news_not_exist'), 'class' => "erreur")
                )
            );

            return $this->redirectToRoute('deja_vu_admin_news');
        }

        $form = $this->createFormBuilder()
            ->add('title', TextType::class, array(
                'constraints'   => array(
                    new NotBlank(array('message' => $this->get('translator')->trans('label.title_empty'))),
                    new Length(array('min' => 4, 'minMessage' => $this->get('translator')->trans('label.title_short'))),
                    new Length(array('max' => 40, 'maxMessage' => $this->get('translator')->trans('label.title_long')))
                ),
                'label'         => $this->get('translator')->trans('label.title'),
                'data'          => $news->getTitle(),
            ))
            ->add('message', TextareaType::class, array(
                'constraints'   => array(
                    new NotBlank(array('message' => $this->get('translator')->trans('label.message_empty'))),
                    new Length(array('min' => 10, 'minMessage' => $this->get('translator')->trans('label.message_short')))
                ),
                'data'          => $news->getMessage(),
            ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.modify'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            try {
                $post = $form->getData();
                $news->setTitle($post['title'])
                    ->setMessage($post['message'])
                    ->setEditer($bdd->getRepository('DejaVuBundle:Users')->find($session->get('user')['id']));
                $bdd->flush();

                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.news_updated'), 'class' => "ok")
                    )
                );

                $news = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_news_edit', array(
                        'id'    => $news->getId(),
                    )),
                    $news->getTitle()
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_news_edited',array('%news%' => $news)));

                return $this->redirectToRoute('deja_vu_admin_news');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.news_not_updated'), 'class' => "erreur")
                    )
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_news_not_edited',array('%news%' => $news->getTitle())));
            }
        }

        return $this->render('DejaVuBundle:Default:admin/news_edit.html.twig', array(
            'form'              => $form->createView(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/news/delete/{id}
     */
    public function deleteAction(Request $request, $id) {
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        $news = $bdd->getRepository('DejaVuBundle:News')->getNewsByIdAdmin($id);

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.delete'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            try {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.news_deleted'), 'class' => "ok")
                    )
                );

                $news_log = $this->get('app.log')->urlForLog(
                    $this->get('router')->generate('deja_vu_admin_news_delete', array(
                        'id'    => $news->getId(),
                    )),
                    $news->getTitle()
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_news_deleted',array('%news%' => $news_log)));

                $bdd->remove($news);
                $bdd->flush();

                return $this->redirectToRoute('deja_vu_admin_news');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.news_not_updated'), 'class' => "erreur")
                    )
                );
                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_news_not_deleted',array('%news%' => $news->getTitle())));
            }
        } // todo

        return $this->render('DejaVuBundle:Default:admin/news_delete.html.twig', array(
            'form'              => $form->createView(),
            'title'             => $news->getTitle(),
            'id'                => $id,
        ));
    }

}

