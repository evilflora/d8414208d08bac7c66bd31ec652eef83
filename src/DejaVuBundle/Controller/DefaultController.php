<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\BannedIp;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /*
     * Route nomdusite.fr/
     */
    public function indexAction()
    {
        return $this->render('DejaVuBundle:Default:news.html.twig');
    } // GOOD

    /*
     * Pas de route mais génère le menu via la session
     */
    public function menuAction(Request $request)
    {
        $session    = $request->getSession();
        $bdd        = $this->getDoctrine()->getManager();

        if(!$bdd->getRepository('DejaVuBundle:BannedIp')->findOneBy(array('ip' => $request->getClientIp()))) {
            $ip = new BannedIp();
            $ip->setIp($request->getClientIp());
            $bdd->persist($ip);
            $bdd->flush();
        }
        if ($session->get('user')['id']) {

            $get_user = $this->getDoctrine()
                ->getManager()
                ->getRepository('DejaVuBundle:Users')
                ->find($session->get('user')['id']);


            $session->set('user', array(
                'id' => $get_user->getId(),
                'pseudo' => $get_user->getPseudo(),
                'registrationDate' => $get_user->getRegistrationDate(),
                'email' => $get_user->getEmail(),
                'title' => array(
                    'id' => $get_user->getTitle()->getId(),
                    'name' => $get_user->getTitle()->getName(),
                ),
            ));

            $ip = $bdd->getRepository('DejaVuBundle:BannedIp')->findOneBy(array('ip' => $request->getClientIp()));
            $get_user->setIp($ip);
            $bdd->flush();
        }

        return $this->render('DejaVuBundle:Default:menu.html.twig', array(
            'session' => $session
        ));
    } // GOOD
}
