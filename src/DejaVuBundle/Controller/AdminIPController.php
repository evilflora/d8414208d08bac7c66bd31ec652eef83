<?php

namespace DejaVuBundle\Controller;

use DejaVuBundle\Entity\BannedIp;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Config\Definition\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminIPController extends Controller
{
    public function viewAction(Request $request){
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        return $this->render('DejaVuBundle:Default:admin/ip.html.twig', array(
            'ips'             => $bdd->getRepository('DejaVuBundle:BannedIp')->findAll(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/ip/bann/{ip}
     */
    public function bannAction(Request $request, $ip)
    {
        $readonly = $user = false;
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if (!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        if ($ip) $readonly = true;

        $form = $this->createFormBuilder()
            ->add('ip', TextType::class, array(
                'constraints' => array(
                    new Regex(array(
                        'pattern' => '/^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/',
                        'message' => $this->get('translator')->trans('label.not_ip'),
                    )),
                ),
                'attr' => array(
                    'readonly' => $readonly
                ),
                'label' => $this->get('translator')->trans('label.ip'),
                'data' => $ip,
            ))
            ->add('message', TextareaType::class, array(
                'constraints' => array(
                    new NotBlank(array('message' => $this->get('translator')->trans('label.message_empty'))),
                    new Length(array('min' => 10, 'minMessage' => $this->get('translator')->trans('label.message_short')))
                ),
                'label' => $this->get('translator')->trans('label.message'),
                'attr' => array(
                    'rows' => '5',
                    'cols' => '50'
                )

            ))
            ->add('save', SubmitType::class, array(
                'label' => $this->get('translator')->trans('label.bann_ip'),
            ))
            ->getForm();

        $form->handleRequest($request);
        $post = $form->getData();

        if ($ip) $user = $bdd->getRepository('DejaVuBundle:Users')->getUsersByIpAdmin($ip);
        if (($post['ip'])) $user = $bdd->getRepository('DejaVuBundle:Users')->getUsersByIpAdmin($post['ip']);

        if ($user) {
            if ($user->getId() == $session->get('user')['id']) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message' => $this->get('translator')->trans('label.not_yourself'),
                            'class' => "erreur",
                        )
                    )
                );
                return $this->redirectToRoute('deja_vu_admin_management_ip');
            }
            if ($user->getTitle()->getId() <= $session->get('user')['title']['id']) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message' => $this->get('translator')->trans('label.ip_better_rank'),
                            'class' => "erreur",
                        )
                    )
                );
                return $this->redirectToRoute('deja_vu_admin_management_ip');
            }
            if ($user->getIp()->getBanned()) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message' => $this->get('translator')->trans('label.ip_already_banned'),
                            'class' => "ok",
                        )
                    )
                );
                return $this->redirectToRoute('deja_vu_admin_management_ip');
            }
        }

        if ($form->isValid() && $form->isSubmitted()) {
            if (!$ip) {
                $newip = $bdd->getRepository('DejaVuBundle:BannedIp')->findOneBy(array('ip' => $post['ip']));
                if(!$newip) {
                    $newip = new BannedIp();
                    $newip->setIp($post['ip']);
                    $bdd->persist($newip);
                    $bdd->flush();
                }
            }
            else {
                $newip = $bdd->getRepository('DejaVuBundle:BannedIp')->findOneBy(array('ip' => $ip));
            }

            try {
                $newip->setBanned($post['message']);
                $bdd->flush();

                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message' => $this->get('translator')->trans('label.ip_banned'),
                            'class' => "ok",
                        )
                    )
                );

                $this->get('app.log')->addLog($request, $this->get('translator')->trans('label.log_ip_banned', array(
                    '%ip%' => $newip->getIp()
                ))
                );

                return $this->redirectToRoute('deja_vu_admin_management_ip');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.ip_not_banned'), 'class' => "erreur")
                    )
                );

                $this->get('app.log')->addLog($request, $this->get('translator')->trans('label.log_ip_not_banned', array(
                    '%ip%' => $newip->getIp()
                )));
            }
        }

        return $this->render('DejaVuBundle:Default:admin/bann_ip.html.twig', array(
            'form' => $form->createView(),
        ));
    } // GOOD

    /*
     * Route nomdusite.fr/admin/ip/unbann/{ip}
     */
    public function unbannAction(Request $request,$ip){
        $readonly = false;
        $session = $request->getSession();
        $bdd = $this->getDoctrine()->getManager();

        if(!$session->get('user')['id'] || $session->get('user')['title']['id'] > 2) // Si on est pas connecté ou non admin
            return $this->redirectToRoute('deja_vu_home');

        if ($ip) {$user = $bdd->getRepository('DejaVuBundle:Users')->getUsersByIpAdmin($ip); $readonly = true; } // todo

        if($ip && $user) {
            if ($user->getId() == $session->get('user')['id']) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.not_yourself'),
                            'class'         => "erreur",
                        )
                    )
                );
                return $this->redirectToRoute('deja_vu_admin_management_ip');
            }
            if ($user->getTitle()->getId() <= $session->get('user')['title']['id']) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.ip_better_rank'),
                            'class'         => "erreur",
                        )
                    )
                );
                return $this->redirectToRoute('deja_vu_admin_management_ip');
            }
            if (!$user->getIp()->getBanned()) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.ip_already_unbanned'),
                            'class'         => "ok",
                        )
                    )
                );
                return $this->redirectToRoute('deja_vu_admin_management_ip');
            }
        }

        $ip = $bdd->getRepository('DejaVuBundle:BannedIp')->findOneBy(array('ip' => $ip));

        $form = $this->createFormBuilder()
            ->add('ip', TextType::class, array(
                'constraints'   => array(
                    new Regex(array(
                        'pattern' => '/^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/',
                        'message' => $this->get('translator')->trans('label.not_ip'),
                    )),
                ),
                'attr'          => array(
                    'readonly'  => $readonly
                ),
                'label'         => $this->get('translator')->trans('label.ip'),
                'data'          => $ip->getIp(),
            ))
            ->add('message', TextareaType::class, array(
                'constraints'   => array(
                    new NotBlank(array('message' => $this->get('translator')->trans('label.message_empty'))),
                    new Length(array('min' => 10, 'minMessage' => $this->get('translator')->trans('label.message_short')))
                ),
                'label'         => $this->get('translator')->trans('label.message'),
                'attr'          => array(
                    'rows'      => '5',
                    'cols'      => '50'
                ),
                'data'          => $ip->getBanned(),

            ))
            ->add('save', SubmitType::class, array(
                'label'         => $this->get('translator')->trans('label.unbann_ip'),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            try {
                $post = $form->getData();
                if(!$ip) $ip = $bdd->getRepository('DejaVuBundle:BannedIp')->find($post['ip']);
                $ip->setBanned(null);
                $bdd->flush();

                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig', array(
                            'message'       => $this->get('translator')->trans('label.ip_unbanned'),
                            'class'         => "ok",
                        )
                    )
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_ip_unbanned',array(
                    '%ip%'        => $ip->getIp()
                ))
                );

                return $this->redirectToRoute('deja_vu_admin_management_ip');
            } catch (Exception $e) {
                $this->addFlash('alert',
                    $this->renderView('DejaVuBundle:Default:_alert.html.twig',
                        array('message' => $this->get('translator')->trans('label.ip_not_unbanned'), 'class' => "erreur")
                    )
                );

                $this->get('app.log')->addLog($request,$this->get('translator')->trans('label.log_ip_not_unbanned',array(
                    '%ip%'        => $ip->getIp()
                ))
                );

            }
        }

        return $this->render('DejaVuBundle:Default:admin/unbann_ip.html.twig', array(
            'form'              => $form->createView(),
        ));
    } // GOOD

}
