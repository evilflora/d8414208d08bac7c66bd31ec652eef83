<?php
namespace DejaVuBundle\Twig;

use Twig_SimpleFilter;

class ConvertUrl extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('convertUrl', array($this, 'stringToUrl')),
        );
    }

    public function stringToUrl($str, $encoding='utf-8') {
        $str = iconv($encoding, 'ASCII//TRANSLIT', $str);
        $str = preg_replace('/[^a-zA-Z0-9\/_|+ -]/', '', $str);
        $str = strtolower(trim($str, '-'));
        $str = preg_replace('/[\/_|+ -]+/', '-', $str);
        return strtolower($str);
    }

    public function getName()
    {
        return 'convert_url';
    }
}
